/**
 * @file
 * Script to build the theme.
 */

/* eslint-env node, es6 */
/* global Promise */
/* eslint-disable key-spacing, one-let, no-multi-spaces, max-nested-callbacks, quote-props */
/* eslint strict: ["error", "global"] */

let importOnce = require('node-sass-import-once');
let path = require('path');
let env = process.env.NODE_ENV || 'testing';
let isProduction = (env === 'production');

let options = {};

// Edit these paths and options.
// The root paths are used to construct all the other paths in this
// configuration. The "project" root path is where this gulpfile.js is located.
// While Zen distributes this in the theme root folder, you can also put this
// (and the package.json) in your project's root folder and edit the paths
// accordingly.
options.rootPath = {
  project : __dirname + '/',
  src : __dirname + '/src/',
  dist : __dirname + '/dist/',
  theme: __dirname + '/',
  drupal : __dirname + '../../../../core/'
};

options.theme = {
  name : 'karabas_theme',
  root : options.rootPath.theme,
  components : options.rootPath.src + 'sass/',
  build : options.rootPath.dist,
  css : options.rootPath.dist + 'css/',
  js : options.rootPath.src + 'js/',
  node : options.rootPath.theme + 'node_modules/',
  images     : options.rootPath.theme + 'images/'
};

// Converts module names to absolute paths for easy imports.
function sassModuleImporter(url, file, done) {
  try {
    let pathResolution = require.resolve(url);
    return done({
      file: pathResolution
    });
  }
  catch (e) {
    return null;
  }
}

// Define the node-sass configuration. The includePaths is critical!
options.sass = {
  importer: [sassModuleImporter, importOnce],
  includePaths: [options.theme.components, options.theme.node],
  outputStyle: (isProduction ? 'compresssed' : 'expanded')
};

let $fonts = [];

// Build CSS.
let sassFiles = [
  options.theme.node + 'slicknav/dist/slicknav.css',
  options.theme.node + 'slick-carousel/slick/slick.css',
  options.theme.node + 'aos/dist/aos.css',
  options.theme.components + '**/*.s*ss',
  // Do not open Sass partials as they will be included as needed.
  '!' + options.theme.components + '**/_*.s*ss'
];

let $js = [
  options.theme.node + 'slicknav/dist/jquery.slicknav.min.js',
  options.theme.node + 'slick-carousel/slick/slick.min.js',
  options.theme.node + 'inputmask/dist/min/jquery.inputmask.bundle.min.js',
  options.theme.node + 'aos/dist/aos.js',
  options.theme.js +    'main.js'
];

// Define which browsers to add vendor prefixes for.
options.autoprefixer = {
  grid: true,
  overrideBrowserslist: [
    '> 1%'
  ]
};

// Define the paths to the JS files to lint.
options.eslint = {
  files : [
    options.rootPath.project + 'gulpfile.js',
    options.theme.js + '**/*.js',
    '!' + options.theme.js + '**/*.min.js',
    options.theme.components + '**/*.js',
    '!' + options.theme.build + '**/*.js'
  ]
};

let $                = require('gulp-load-plugins')();
let browserSync      = require('browser-sync').create();
let concat           = require('gulp-concat');
let fs               = require('fs');
let gulp             = require('gulp');
let gulpif           = require('gulp-if');
let del              = require('del');
let cache            = require('gulp-cached');
let prompt           = require('prompt');
// let rename           = require('gulp-rename');
let sass             = require('gulp-sass');
let sourcemaps       = require('gulp-sourcemaps');
let spritesmith      = require('gulp.spritesmith');


/* Local setup */
gulp.task('config', function (cb) {
  prompt.get('target', function (err, result) {
    if (!err) {
      fs.writeFileSync(path.join(__dirname, 'config.json'), JSON.stringify(result));
    }
    cb();
  });
});

/* Sprites */
gulp.task('sprites', function () {
  return  gulp.src(options.theme.images + 'icons/*.png')
    .pipe(spritesmith({
      imgName: 'sprite.png',
      cssName: '_sprite.scss',
      padding: 1
    }))
    .pipe(gulpif('*.png', gulp.dest(options.rootPath.dist)))
    .pipe(gulpif('*.scss', gulp.dest(options.theme.components + 'mixins/')));
});

/* Fonts */
gulp.task('fonts', function () {
  return  gulp.src($fonts)
    .pipe(gulp.dest(options.theme.root + 'fonts'));
});

/* JS concat */
gulp.task('js_concat', ['lint:js'], function () {
  gulp.src($js)
    .pipe(sourcemaps.init())
    .pipe(concat('main.js'))
    .pipe(gulp.dest('./dist/js'));
});

// Lint Sass and JavaScript.
gulp.task('lint', ['lint:sass', 'lint:js']);

// Lint JavaScript.
gulp.task('lint:js', function () {
  return gulp.src(options.eslint.files)
    .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.eslint.failOnError());
});

// Lint Sass.
gulp.task('lint:sass', function () {
  return gulp.src(options.theme.components + '**/*.scss')
    .pipe($.sassLint())
    .pipe($.sassLint.format())
    .pipe($.sassLint.failOnError());
});

/* Main SASS task */
gulp.task('styles', ['sprites'], function () {
  return gulp.src(sassFiles)
    .pipe($.if(!isProduction, $.sourcemaps.init()))
    .pipe($.if(!isProduction, cache()))
    .pipe(sass(options.sass).on('error', sass.logError))
    .pipe($.autoprefixer(options.autoprefixer))
    .pipe($.rename({dirname: ''}))
    .pipe($.size({showFiles: true}))
    .pipe($.if(!isProduction,  $.sourcemaps.write('./')))
    .pipe(gulp.dest(options.theme.css))
    .pipe($.if(browserSync.active, browserSync.stream({match: '**/*.css'})));
});

// Clean all directories.
gulp.task('clean', ['clean:css', 'clean:dist']);

// Clean CSS files.
gulp.task('clean:css', function () {
  return del([
    options.theme.css + '**/*.css',
    options.theme.css + '**/*.map'
  ], {force: true});
});

// Clean `dist` folder.
gulp.task('clean:dist', function () {
  return del([
    options.rootPath.dist + '/**/*'
  ], {force: true});
});

// Watch for changes and rebuild.
gulp.task('watch', ['browser-sync', 'watch:js', 'watch:css']);

gulp.task('browser-sync', ['watch:css'], function () {
  if (!options.drupalURL) {
    return Promise.resolve();
  }
  return browserSync.init({
    proxy: options.drupalURL,
    noOpen: true
  });
});

gulp.task('watch:css', ['styles'], function () {
  return gulp.watch(options.theme.components + '**/*.scss', options.gulpWatchOptions, ['styles']);
});

gulp.task('watch:js', ['lint:js', 'js_concat'], function () {
  return gulp.watch(options.eslint.files, options.gulpWatchOptions, ['lint:js', 'js_concat']);
});

/* Gulp compile */
gulp.task('compile', ['styles', 'js_concat', 'fonts']);

/* Default task */
gulp.task('default', ['compile']);
