/* eslint-disable no-console */
(function (arr) {
  arr.forEach(function (item) {
    if (item.hasOwnProperty('remove')) {
      return;
    }
    Object.defineProperty(item, 'remove', {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function remove() {
        if (this.parentNode === null) {
          return;
        }
        this.parentNode.removeChild(this);
      }
    });
  });
})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);

(function ($) {
  'use strict';

  function removePreloader() {
    $('.preloader-wrapper').remove();
  }
  setTimeout(removePreloader, 3200);

  document.addEventListener('DOMContentLoaded', function () {
    document.querySelector('.preloader-wrapper').classList.add('hide');
  });

  Drupal.behaviors.slider = {
    attach: function (context, settings) {
      var opt = [
        {
          slider: $('.main-slider .field--name-field-slides'),
          options: {
            arrows: false,
            autoplay: true,
            fade: true,
            autoplaySpeed: 2000,
            infinite: true,
            slidesToScroll: 1,
            slidesToShow: 1,
            speed: 300,
            cssEase: 'linear',
            variableHeight: true
          }
        },
        {
          slider: $('.instagram-wrapper'),
          options: {
            dots: false,
            infinite: true,
            speed: 300,
            arrows: true,
            slidesToShow: 6,
            variableWidth: true,
            initialSlide: 0,
            centerMode: true
          }
        },
        {
          slider: $('.facebook-posts-block'),
          options: {
            slidesToScroll: 1,
            arrows: true,
            touchMove: true,
            slidesToShow: 3,
            cssEase: 'linear',
            variableHeight: true,
            responsive: [
              {
                breakpoint: 960,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1
                }
              },
              {
                breakpoint: 767,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  fade: true
                }
              }
            ]
          }
        }
      ];

      for (var i = 0; i < opt.length; i++) {
        this.initialize(opt[i].slider, opt[i].options);
      }
    },
    initialize: function (slider, options) {
      slider.not('.slick-initialized').slick(options);
    }
  };

  Drupal.behaviors.BurgerMenu = {
    attach: function (context, settings) {
      var body = $('body', context).once();
      var options = [
        {
          burger: $('.ham', context).once(),
          blocks: $('.field--name-karabas-main-menu > .menu', context)
        }
      ];
      for (var i = 0; i < options.length; i++) {
        this.menuListener(options[i].burger, options[i].blocks, body);
      }
    },
    menuListener: function (burger, blocks, body) {
      var self = this;

      burger.on('click', function () {
        if (body.hasClass('burger--open')) {
          self.closeBurgerMenu(blocks, body);
        }
        else {
          self.openBurgerMenu(blocks, body);
        }
      });
    },
    openBurgerMenu: function (blocks, body) {
      body.addClass('burger--open');
      blocks.addClass('open').slideDown();
    },
    closeBurgerMenu: function (blocks, body) {
      body.removeClass('burger--open');
      blocks.removeClass('open').slideUp();
    }
  };

  Drupal.behaviors.overlay = {
    attach: function (context, settings) {
      this.overlayFunc('.ham', '.overlay', '.field--name-karabas-main-menu .menu-item > a', 'body', '.field--name-karabas-main-menu > .menu', context);
    },
    overlayFunc: function (btn, overlay, menupoint, body, menu, context) {
      $(overlay, context).click(function () {
        $(btn, context).click();
      });

      $(document).bind('keydown', function (e) {
        if (e.which === 27) {
          if ($(body).hasClass('burger--open')) {
            $(btn, context).click();
          }
        }
      });

      $(menupoint, context).click(function () {
        $(body).removeClass('burger--open');
        $(menu).removeClass('open');
      });
    }
  };

  Drupal.behaviors.smoothScroll = {
    attach: function (context, settings) {

      $('a[href*="#"]', context).on('click', function (event) {
        const target = $($(this).attr('href'));
        event.preventDefault();

        const elem_position = target.offset().top;
        const window_height = $(window).height();
        const y = elem_position - window_height / 5;

        $('html, body').animate({scrollTop: y}, 600);
      });
      /* eslint-disable */
    }
  };

  Drupal.behaviors.video = {
    youtubeAPIStatus: false,
    $iframeElement: null,
    attach: function attach(context) {
      var _this = this;

      var youtubeIframeId = 'youtube-iframe';
      $('.main-slider .field--name-field-slides', context).on('afterChange', function (event, slick) {
        var $currentSlide = $('.slick-current', event.target);

        if ($currentSlide.find('.silde-type-video').length && _this.youtubeAPIStatus && $(window).width() >= 920) {
          var container = $currentSlide.find('.youtube-iframe-container');
          slick.pause();
          _this.$iframeElement && _this.$iframeElement.remove();
          _this.$iframeElement = $('<div />').attr('id', youtubeIframeId);
          $currentSlide.find('.youtube-iframe-container').append(_this.$iframeElement);
          var player = new YT.Player(youtubeIframeId, {
            videoId: container.attr('data-id'),
            playerVars: {
              controls: 0,
              disablekb: 1,
              autoplay: 0,
              autohide: 1,
              modestbranding: 0,
              rel: 0,
              showinfo: 0,
              enablejsapi: 0,
              frameborder: 0,
              iv_load_policy: 3
            },
            events: {
              onReady: function onReady(e) {
                //                  this.appendPoster($currentSlide, container.attr('data-id'));
                e.target.playVideo();
                e.target.mute();

                _this.vidRescale();
              },
              onStateChange: function onStateChange(e) {
                if (e.data === YT.PlayerState.ENDED) {
                  slick.play();
                  player.destroy();

                  _this.$iframeElement.remove();
                }
              }
            }
          });
        }
      });
    },
    updateYoutubeStatus: function updateYoutubeStatus() {
      this.youtubeAPIStatus = true;
    },
    createYoutubeScript: function createYoutubeScript() {
      var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    },
    vidRescale: function vidRescale() {
      var w = $('.field--name-karabas-video').width() + 200,
        h = $('.field--name-karabas-video').height() + 200;
      var iframe = $('#youtube-iframe');

      if (w / h > 16 / 9) {
        iframe.width(w);
        iframe.height(w / 16 * 9);
        iframe.css({
          'left': '0px'
        });
      } else {
        iframe.width(h / 9 * 16);
        iframe.height(h);
        iframe.css({
          'left': -($('#youtube-iframe').outerWidth() - w) / 2
        });
      }
    }
  };
  window.addEventListener('resize', function () {
    return Drupal.behaviors.video.vidRescale();
  });

  window.onYouTubeIframeAPIReady = function () {
    Drupal.behaviors.video.updateYoutubeStatus();
  };

  Drupal.behaviors.video.createYoutubeScript();

  Drupal.behaviors.animationOnScroll = {
    attach: function (context, settings) {
      // eslint-disable-next-line no-undef
      AOS.init({
        offset: 120,
        delay: 0,
        disable: function () {
          var maxWidth = 970;
          return window.innerWidth < maxWidth;
        },
        easing: 'ease',
        duration: 400,
        once: false,
        startEvent: 'DOMContentLoaded'
      });
    }
  };


  Drupal.behaviors.cartLinkReplace = {
    attach: function (context, settings) {
      this.cartLinkReplace (
        '.site-header .ham',
        '.field--name-karabas-main-menu .menu',
        '.paragraph--type--korzina', context);
    },
    cartLinkReplace : function (header, menu, cart, context) {
      var $header = $(header, context);
      var $menu = $(menu, context);
      var $cart = $(cart, context);
      var ww = $(window).width();

      if (ww < 1170) {
        $cart.insertAfter($header)
      }
      else {
        $cart.appendTo($menu)
      }

      $(window).resize(function () {
        var windowWidth = $(window).width();
        if (windowWidth < 1170) {
          $cart.insertAfter($header)
        }
        else {
          $cart.appendTo($menu)
        }
      })
    }
  };

  Drupal.behaviors.inputMask = {
    attach: function (context, settings) {
      var phone = [
        {
          selector: 'input[type=tel]',
          context:context,
          options: {
            'mask': '+380 (99)99-99-999'
          }
        }
      ]

      phone.forEach(function (el) {
        $(el.selector).inputmask(el.options)
      })
    }
  };

  Drupal.behaviors.removeScroll = {
    attach: function (context, settings) {

      const body = document.querySelector('body')

      if(context.length > 0 || context.className === 'additionals-wrapper') {
        body.classList.add('remove-scroll')
      }

      document.addEventListener('click', ev => {
        if (ev.target.classList.contains('ui-icon-closethick')) {
          body.classList.remove('remove-scroll')
        }
      })

      return
    }
  }

  Drupal.behaviors.deliveryItems = {
    attach: function (context, settings) {
      this.deliveryItems (
        '.product-block-container .product-item a',
        '.product-sidebar-menu-container',
        '.product-sidebar-menu-container .drop-down-content',
        '.product-sidebar-menu-container .drop-down-content a',context);
    },
    deliveryItems : function (productItem, sidebarMenu, dropDownContent, dropDownContentItem, context) {
      var $productItem = $(productItem, context);
      var $sidebarMenu = $(sidebarMenu, context);
      var $dropDownContent = $(dropDownContent, context);
      var $dropDownContentItem = $(dropDownContentItem, context);

      if ($('.product-sidebar-menu-container').length) {
        $(document).scroll(function () {
          var sidebarOffset = $('.product-sidebar-menu-container').offset().top

          var menuOffset = window.pageYOffset + document.querySelector(
            '.product-block-container')
            .getBoundingClientRect().bottom - 50

          if(sidebarOffset > menuOffset) {
            $sidebarMenu.addClass('show')
          }
          else {
            $sidebarMenu.removeClass('show')
          }
        })
      }

      $productItem.click(function () {
        $productItem.removeClass('active')
        $(this).toggleClass('active')

        scrollToBlock()
      })

      $dropDownContent.hide()
      $sidebarMenu.click(function () {
        $dropDownContent.slideToggle()
        $sidebarMenu.toggleClass('active')

        if($sidebarMenu.hasClass('active')) {
          $('body').addClass('remove-scroll')
        }
        else {
          $('body').removeClass('remove-scroll')
        }
      })

      $dropDownContentItem.click(function () {
        $dropDownContent.hide()
        $dropDownContentItem.removeClass('active')
        $sidebarMenu.removeClass('active')
        $productItem.removeClass('active')
        $('body').removeClass('remove-scroll')
        $(this).addClass('active')

        scrollToBlock()
      })

      function scrollToBlock() {
        var target = $('.product-block-catalog');

        var elem_position = target.offset().top;
        var window_height = $(window).height();
        var y = elem_position - window_height / 4 - 10;

        $('html, body').animate({scrollTop: y}, 1000);
      }
    }
  };

  Drupal.behaviors.jumpContent = {
    attach: function (context, settings) {
      setTimeout(function () {
        var block = $('.basic-cart-cart-form')
        var productBlock = $('.product-block-catalog')
        var minHeight = block.outerHeight()
        var productMinHeight = productBlock.outerHeight()

        block.css('min-height', minHeight)
        productBlock.css('min-height', productMinHeight)

      }, 600)
      return
    }
  };

  Drupal.behaviors.additionalsReplace = {
    attach: function (context, settings) {
      var $additionals = $('.product-block-catalog .node--type-client-order + a');

      $additionals.each(function (i, el) {
        var $wrapper = $(el).prev().find('.field--name-field-image');

        $(el).insertAfter($wrapper)
      })
    }
  };

})(jQuery);