<?php

use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Implements hook_preprocess_block().
 */
function karabas_theme_preprocess_block(&$variables) {
  switch ($variables['base_plugin_id']) {
    case 'system_branding_block':
      $variables['site_logo'] = '';
      if ($variables['content']['site_logo']['#access'] && $variables['content']['site_logo']['#uri']) {
        $variables['site_logo'] = str_replace('.svg', '.png', $variables['content']['site_logo']['#uri']);
      }
      break;

  }
}

/**
 * Implements hook_preprocess_ds_entity_view().
 */
function karabas_theme_preprocess_ds_entity_view(&$variables) {
  if ($variables['content']['#entity_type'] === 'paragraph' && $variables['content']['#bundle'] === 'title_banner' && $variables['content']['#view_mode'] === 'default') {
    /* @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    $paragraph = $variables['content']['#paragraph'];
    if ($image_id = $paragraph->field_image->target_id) {
      $image = File::load($image_id);

      $variables['content']['#attributes']['style'] = 'background-image: url(' . $image->url() . ')';
      $variables['content']['#attached']['library'][] = 'karabas_theme/title-banner';
      $variables['content']['#attached']['library'][] = 'karabas_theme/slider-banner';
    }
  }

  if ($variables['content']['#entity_type'] === 'paragraph' && $variables['content']['#bundle'] === 'footer') {
    $variables['content']['#attached']['library'][] = 'karabas_theme/footer-titledescription';
    $variables['content']['#attached']['library'][] = 'karabas_theme/facebook';
    $variables['content']['#attached']['library'][] = 'karabas_theme/slick-dots';
    $variables['content']['#attached']['library'][] = 'karabas_theme/instagram';
    $variables['content']['#attached']['library'][] = 'karabas_theme/social-links';
    $variables['content']['#attached']['library'][] = 'karabas_theme/loaders';
    $variables['content']['#attached']['library'][] = 'karabas_theme/main-slider';
    $variables['content']['#attached']['library'][] = 'karabas_theme/login-form';
    $variables['content']['#attached']['library'][] = 'karabas_theme/captcha';
    $variables['content']['#attached']['library'][] = 'karabas_theme/youtube';
    $variables['content']['#attached']['library'][] = 'karabas_theme/footer-logo-dev-company';
  }

  if ($variables['content']['#entity_type'] === 'paragraph' && $variables['content']['#bundle'] === 'banner_with_images') {
    $variables['content']['#attached']['library'][] = 'karabas_theme/banner-with-image';
    $variables['content']['#attached']['library'][] = 'karabas_theme/delivery-items-popup';
    $variables['content']['#attached']['library'][] = 'karabas_theme/delivery-items';
    $variables['content']['#attached']['library'][] = 'karabas_theme/cart-popup';
    $variables['content']['#attached']['library'][] = 'karabas_theme/order-form';
    $variables['content']['#attached']['library'][] = 'karabas_theme/delivery-textblock';
    $variables['content']['#attached']['library'][] = 'karabas_theme/sticky-header-and-cart';
  }

  if ($variables['content']['#entity_type'] === 'paragraph' && $variables['content']['#bundle'] === 'slide') {
    $variables['content']['#attached']['library'][] = 'karabas_theme/slick-slider';
  }
}

/**
 * Menu preprocess.
 */
function karabas_theme_preprocess_menu(&$variables) {
  switch ($variables['menu_name']) {
    case 'main':
      $variables['#attached']['library'][] = 'karabas_theme/main-menu';
      $variables['#attached']['library'][] = 'karabas_theme/slicknav-menu';
      break;
  }
}

/**
 * Implement hook_preprocess_file_link().
 */
function karabas_theme_preprocess_file_link(&$variables) {
  $file = $variables['file'];
  $options = [];

  $file_entity = ($file instanceof File) ? $file : File::load($file->fid);
  // @todo Wrap in file_url_transform_relative(). This is currently
  // impossible. As a work-around, we currently add the 'url.site' cache context
  // to ensure different file URLs are generated for different sites in a
  // multisite setup, including HTTP and HTTPS versions of the same site.
  // Fix in https://www.drupal.org/node/2646744.
  $url = file_create_url($file_entity->getFileUri());

  $mime_type = $file->getMimeType();
  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options['attributes']['type'] = $mime_type . '; length=' . $file->getSize();

  // Use the description as the link text if available.
  if (empty($variables['description'])) {
    $link_text = $file_entity->getFilename();
  }
  else {
    $link_text = $variables['description'];
    $options['attributes']['title'] = $file_entity->getFilename();
  }

  $options['attributes']['target'] = '_blank';

  $variables['link'] = \Drupal::l($link_text, Url::fromUri($url, $options));
}

/**
 * Delete message status
 *
 * @param $variables
 */
function karabas_theme_preprocess_status_messages(&$variables) {
  if (isset($variables['message_list']['status'])) {
    unset($variables['message_list']['status']);
  }
}
