include .env

install:
	docker-compose build; \
	docker-compose up -d; \
	docker-compose exec php composer install; \
	docker-compose exec php php ./vendor/bin/drush si minimal \
		--db-url=${DB_DRIVER}://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:3306/${DB_NAME} \
		--account-name=root \
		--account-pass=secret \
		--config-dir='./config/sync' \
		--yes; \
	docker-compose exec php php ./vendor/bin/drush cim -y; \
	docker-compose exec php php ./vendor/bin/drush cr;

import_configs:
	docker-compose exec php composer install; \
	docker-compose exec php php ./vendor/bin/drush cim -y; \
	docker-compose exec php php ./vendor/bin/drush updb -y; \
	docker-compose exec php php ./vendor/bin/drush cr;

export_configs:
	docker-compose exec php php ./vendor/bin/drush cex -y;
