<?php

namespace Drupal\karabas_telegram\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class TelegramSettingsForm extends ConfigFormBase{

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return "telegram_settings_form";
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'karabas_telegram.settings',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('karabas_telegram.settings');

    $form['settings']['chat_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Chat ID'),
      '#default_value' => $config->get('chat_id'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('karabas_telegram.settings')
      ->set('chat_id', $form_state->getValue('chat_id'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
