<?php

namespace Drupal\karabas_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\Console\Command\Shared\TranslationTrait;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * @ParagraphsBehavior(
 *   id = "karabas_paragraphs_aos",
 *   label = @Translation("AOS"),
 *   description = @Translation("Add AOS anmations"),
 *   weight = 0,
 * )
 */
class AOSParagraphBehavior extends ParagraphsBehaviorBase {

  use TranslationTrait;

  const ANIMATION_TYPE_FIELD = 'animation_type';
  const OFFSET_FIELD = 'offset';
  const DELAY_FIELD = 'delay';
  const DURATION_FIELD = 'duration';
  const EASING_FIELD = 'easing';

  /**
   * Array of Animations.
   *
   * @var string[]
   */
  private $animations_type = [
    '_none' => 'none',
    'fade' => 'fade',
    'fade-up' => 'fade-up',
    'fade-down' => 'fade-down',
    'fade-left' => 'fade-left',
    'fade-right' => 'fade-right',
    'fade-up-right' => 'fade-up-right',
    'fade-up-left' => 'fade-up-left',
    'fade-down-right' => 'fade-down-right',
    'fade-down-left' => 'fade-down-left',
    'flip-up' => 'flip-up',
    'flip-down' => 'flip-down',
    'flip-left' => 'flip-left',
    'flip-right' => 'flip-right',
    'slide-up' => 'slide-up',
    'slide-down' => 'slide-down',
    'slide-left' => 'slide-left',
    'slide-right' => 'slide-right',
    'zoom-in' => 'zoom-in',
    'zoom-in-up' => 'zoom-in-up',
    'zoom-in-down' => 'zoom-in-down',
    'zoom-in-left' => 'zoom-in-left',
    'zoom-in-right' => 'zoom-in-right',
    'zoom-out' => 'zoom-out',
    'zoom-out-up' => 'zoom-out-up',
    'zoom-out-down' => 'zoom-out-down',
    'zoom-out-left' => 'zoom-out-left',
    'zoom-out-right' => 'zoom-out-right',
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      self::ANIMATION_TYPE_FIELD . '_left' => key($this->animations_type),
      self::OFFSET_FIELD . '_left' => '',
      self::DELAY_FIELD . '_left' => '',
      self::DURATION_FIELD . '_left' => '',
      self::EASING_FIELD . '_left' => '',
      self::ANIMATION_TYPE_FIELD . '_right' => key($this->animations_type),
      self::OFFSET_FIELD . '_right' => '',
      self::DELAY_FIELD . '_right' => '',
      self::DURATION_FIELD . '_right' => '',
      self::EASING_FIELD . '_right' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return in_array($paragraphs_type->id(), [
      'text',
      'text_block',
      'title_banner',
      'contact_banner',
      'block',
      'delivery',
      'banner_with_images',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $short_summary = [];

    foreach (['left', 'right'] as $item) {
      $type_left = $paragraph->getBehaviorSetting($this->getPluginId(), self::ANIMATION_TYPE_FIELD . '_' . $item);
      $easing = $paragraph->getBehaviorSetting($this->getPluginId(), self::EASING_FIELD . '_' . $item);
      $offset = $paragraph->getBehaviorSetting($this->getPluginId(), self::OFFSET_FIELD . '_' . $item);
      $delay = $paragraph->getBehaviorSetting($this->getPluginId(), self::DELAY_FIELD . '_' . $item);
      $duration = $paragraph->getBehaviorSetting($this->getPluginId(), self::DURATION_FIELD . '_' . $item);

      $short_summary[] = $type_left ? $this->t('[' . $item . ']' . 'Animation type @type', ['@type' => $type_left]) : '';
      $short_summary[] = $easing ? $this->t('[' . $item . ']' . 'Easing @easing', ['@easing' => $easing]) : '';
      $short_summary[] = $offset ? $this->t('[' . $item . ']' . 'Offset @offset', ['@offset' => $offset]) : '';
      $short_summary[] = $delay ? $this->t('[' . $item . ']' . 'Delay @delay', ['@delay' => $delay]) : '';
      $short_summary[] = $duration ? $this->t('[' . $item . ']' . 'Duration @duration', ['@duration' => $duration]) : '';
    }

    return $short_summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    foreach (['left', 'right'] as $item) {
      $type = $paragraph->getBehaviorSetting($this->getPluginId(), self::ANIMATION_TYPE_FIELD . '_' . $item);
      $easing = $paragraph->getBehaviorSetting($this->getPluginId(), self::EASING_FIELD . '_' . $item);
      $offset = $paragraph->getBehaviorSetting($this->getPluginId(), self::OFFSET_FIELD . '_' . $item);
      $delay = $paragraph->getBehaviorSetting($this->getPluginId(), self::DELAY_FIELD . '_' . $item);
      $duration = $paragraph->getBehaviorSetting($this->getPluginId(), self::DURATION_FIELD . '_' . $item);

      $form[self::ANIMATION_TYPE_FIELD . '_' . $item] = [
        '#type' => 'select',
        '#title' => $this->t('Animations [@position]', ['@position' => $item]),
        '#options' => $this->animations_type,
        '#default_value' => $type,
      ];

      $form[self::EASING_FIELD . '_' . $item] = [
        '#type' => 'select',
        '#title' => $this->t('Easing [@position]', ['@position' => $item]),
        '#options' => [
          '_none' => 'none',
          'linear' => 'linear',
          'ease' => 'ease',
          'ease-in' => 'ease-in',
          'ease-out' => 'ease-out',
          'ease-in-out' => 'ease-in-out',
          'ease-in-back' => 'ease-in-back',
          'ease-out-back' => 'ease-out-back',
          'ease-in-out-back' => 'ease-in-out-back',
          'ease-in-sine' => 'ease-in-sine',
          'ease-out-sine' => 'ease-out-sine',
          'ease-in-out-sine' => 'ease-in-out-sine',
          'ease-in-quad' => 'ease-in-quad',
          'ease-out-quad' => 'ease-out-quad',
          'ease-in-out-quad' => 'ease-in-out-quad',
          'ease-in-cubic' => 'ease-in-cubic',
          'ease-out-cubic' => 'ease-out-cubic',
          'ease-in-out-cubic' => 'ease-in-out-cubic',
          'ease-in-quart' => 'ease-in-quart',
          'ease-out-quart' => 'ease-out-quart',
          'ease-in-out-quart' => 'ease-in-out-quart',
        ],
        '#default_value' => $easing,
      ];

      $form[self::OFFSET_FIELD . '_' . $item] = [
        '#type' => 'textfield',
        '#title' => $this->t('Offset [@position]', ['@position' => $item]),
        '#default_value' => $offset,
      ];

      $form[self::DELAY_FIELD . '_' . $item] = [
        '#type' => 'textfield',
        '#title' => $this->t('Delay [@position]', ['@position' => $item]),
        '#default_value' => $delay,
      ];

      $form[self::DURATION_FIELD . '_' . $item] = [
        '#type' => 'textfield',
        '#title' => $this->t('Duration [@position]', ['@position' => $item]),
        '#default_value' => $duration,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $build['#aos'] = [];

    foreach (['left', 'right'] as $item) {
      $type = $paragraph->getBehaviorSetting($this->getPluginId(), self::ANIMATION_TYPE_FIELD . '_' . $item);
      $easing = $paragraph->getBehaviorSetting($this->getPluginId(), self::EASING_FIELD . '_' . $item);
      $offset = $paragraph->getBehaviorSetting($this->getPluginId(), self::OFFSET_FIELD . '_' . $item);
      $delay = $paragraph->getBehaviorSetting($this->getPluginId(), self::DELAY_FIELD . '_' . $item);
      $duration = $paragraph->getBehaviorSetting($this->getPluginId(), self::DURATION_FIELD . '_' . $item);

      $build['#aos'][$item] = [
        'data-aos' => $type !== '_none' ? $type : '',
        'data-aos-easing' => $easing !== '_none' ? $easing : '',
        'data-aos-offset' => $offset,
        'data-aos-delay' => $delay,
        'data-aos-duration' => $duration,
      ];
    }
  }
}
