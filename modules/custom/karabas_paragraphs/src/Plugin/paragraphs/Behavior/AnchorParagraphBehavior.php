<?php

namespace Drupal\karabas_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\Console\Command\Shared\TranslationTrait;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * @ParagraphsBehavior(
 *   id = "karabas_paragraphs_anchor",
 *   label = @Translation("Anchor"),
 *   description = @Translation("Add behaviors for anchor"),
 *   weight = 0,
 * )
 */
class AnchorParagraphBehavior extends ParagraphsBehaviorBase {

  use TranslationTrait;

  const PLUGIN_ID = 'karabas_paragraphs_anchor';

  const STATUS_FIELD = 'status';

  const TITLE_FIELD = 'title';

  const WEIGHT_FIELD = 'weight';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      self::STATUS_FIELD => FALSE,
      self::TITLE_FIELD => '',
      self::WEIGHT_FIELD => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return in_array($paragraphs_type->id(), [
      'banner_with_background_slider',
      'text',
      'text_block',
      'title_banner',
      'contact_banner',
      'block',
      'delivery',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $status = $paragraph->getBehaviorSetting($this->getPluginId(), self::STATUS_FIELD);
    $title = $paragraph->getBehaviorSetting($this->getPluginId(), self::TITLE_FIELD);
    $weight = $paragraph->getBehaviorSetting($this->getPluginId(), self::WEIGHT_FIELD);

    return [
      $status ? $this->t('Anchor is @status', ['@status' => $status]) : '',
      $title ? $this->t('Menu link title is @title', ['@title' => $title]) : '',
      $weight ? $this->t('Menu link weight is @weight', ['@weight' => $weight]) : '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $weight = $paragraph->getBehaviorSetting($this->getPluginId(), self::WEIGHT_FIELD);

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable anchor'),
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), self::STATUS_FIELD),
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Menu link title'),
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), self::TITLE_FIELD),
    ];

    $form['weight'] = [
      '#type' => 'number',
      '#min' => -100,
      '#max' => 100,
      '#title' => $this->t('Menu link weight'),
      '#default_value' => $weight ? $weight : 0,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $build['#attributes'] = [
      'id' => self::getParagraphId($paragraph),
    ];
  }

  /**
   * Gets id attribute for paragraph item
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   Paragraph entity.
   *
   * @return string
   *   Id attribute.
   */
  public static function getParagraphId(ParagraphInterface $paragraph) {
    return $paragraph->bundle() . '-' . $paragraph->id();
  }

}
