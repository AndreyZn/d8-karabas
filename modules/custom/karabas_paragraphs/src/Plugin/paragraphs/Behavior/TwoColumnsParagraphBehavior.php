<?php

namespace Drupal\karabas_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\Console\Command\Shared\TranslationTrait;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * @ParagraphsBehavior(
 *   id = "karabas_paragraphs_two_columns",
 *   label = @Translation("Two column paragraphs"),
 *   description = @Translation("Add posibility to change image "),
 *   weight = 0,
 * )
 */
class TwoColumnsParagraphBehavior extends ParagraphsBehaviorBase {

  use TranslationTrait;

  const NORMAL = 'normal';
  const VICE_VERSA = 'vice_versa';

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return in_array($paragraphs_type->id(), ['text', 'contact_banner']);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'image_position' => self::NORMAL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $image_position = $paragraph->getBehaviorSetting($this->getPluginId(), 'image_position');

    return [$image_position ? $this->t('Image position @element', ['@element' => $image_position]) : ''];
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['image_position'] = [
      '#type' => 'radios',
      '#title' => $this->t('Image position'),
      '#options' => [
        self::NORMAL => $this->t('Normal'),
        self::VICE_VERSA => $this->t('Vice versa'),
      ],
      '#required' => TRUE,
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'image_position'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $image_position = $paragraph->getBehaviorSetting($this->getPluginId(), 'image_position');

    if ($image_position === self::VICE_VERSA) {
      list(
        $build['#ds_configuration']['regions']['left'],
        $build['#ds_configuration']['regions']['right']
        ) = [
        $build['#ds_configuration']['regions']['right'],
        $build['#ds_configuration']['regions']['left'],
      ];
    }

    return $build;
  }

}
