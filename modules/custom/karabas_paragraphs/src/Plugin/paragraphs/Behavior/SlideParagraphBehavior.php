<?php

namespace Drupal\karabas_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\Console\Command\Shared\TranslationTrait;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * @ParagraphsBehavior(
 *   id = "karabas_paragraphs_slide",
 *   label = @Translation("Slide Paragraph element"),
 *   description = @Translation("Add behaviors for slide type changes"),
 *   weight = 0,
 * )
 */
class SlideParagraphBehavior extends ParagraphsBehaviorBase {

  use TranslationTrait;

  const VIDEO = 'video';
  const IMAGE = 'image';

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return in_array($paragraphs_type->id(), ['slide']);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $slider_type = $paragraph->getBehaviorSetting($this->getPluginId(), 'slider_type');

    return [$slider_type ? $this->t('Slider type: @element', ['@element' => $slider_type]) : ''];
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['slider_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Slide type'),
      '#description' => $this->t('Select slide type'),
      '#options' => [
        self::IMAGE => $this->t('Image'),
        self::VIDEO => $this->t('Video'),
      ],
      '#required' => TRUE,
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'slider_type'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $type = $paragraph->getBehaviorSetting($this->getPluginId(), 'slider_type');

    $build['#attributes'] = [
      'class' => ['silde-type-' . $type],
    ];

    switch ($type) {
      case self::VIDEO:
        unset($build['field_image']);
        break;
      case self::IMAGE:
        unset($build['field_video']);
        break;
    }

    return $build;
  }

}
