<?php

namespace Drupal\karabas_facebook\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Facebook;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'FacebookBlock' block.
 *
 * @Block(
 *  id = "karabas_facebook_block",
 *  admin_label = @Translation("Karabas Facebook block"),
 * )
 */
class FacebookBlock extends BlockBase implements ContainerFactoryPluginInterface {

  protected $renderer;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = $renderer;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'app_id' => $this->t(''),
        'app_secret' => $this->t(''),
        'page_id' => $this->t(''),
        'access_token' => $this->t(''),
        'limit'=> 3,
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['feed_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('FaceBook settings'),
      '#weight' => '3',
    ];

    //=================== ID Application ==========================
    $form['feed_settings']['app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ID Application'),
      '#description' => $this->t('ID your Application'),
      '#default_value' => $this->configuration['app_id'],
      '#maxlength' => 64,
      '#size' => 25,
      '#required' => TRUE,
      '#weight' => '4',
    ];

    $form['feed_settings']['app_id_info'] = [
      '#type' => 'details',
      '#title' => $this->t('What is my ID Application?'),
      '#weight' => '5',
    ];

    $form['feed_settings']['app_id_info']['summary'] = [
      '#markup' => '<p>https://developers.facebook.com/apps</p>',
    ];

    //=================== App Secret ======================
    $form['feed_settings']['app_secret'] = [
      '#type' => 'password',
      '#title' => $this->t('Application Secret Key'),
      '#description' => $this->t('Secret Key your Application'),
      '#default_value' => $this->configuration['app_secret'],
      '#maxlength' => 64,
      '#size' => 50,
      '#required' => TRUE,
      '#weight' => '6',
    ];

    $form['feed_settings']['app_secret_info'] = [
      '#type' => 'details',
      '#title' => $this->t('What is my ID Application Secret Key?'),
      '#weight' => '7',
    ];

    $form['feed_settings']['app_secret_info']['summary'] = [
      '#markup' => '<p>The application secret specified in your Facebook App\'s dashboard page. This field remains blank for security purposes. If you have already saved your application secret, leave this field blank, unless you wish to update it.</p>',
    ];

    //=================== Page ID ==========================
    $form['feed_settings']['page_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page ID'),
      '#description' => $this->t('ID of the page'),
      '#default_value' => $this->configuration['page_id'],
      '#maxlength' => 64,
      '#size' => 25,
      '#required' => TRUE,
      '#weight' => '8',
    ];

    $form['feed_settings']['page_id_info'] = [
      '#type' => 'details',
      '#title' => $this->t('What is my page ID?'),
      '#weight' => '9',
    ];

    $form['feed_settings']['page_id_info']['summary'] = [
      '#markup' => '<p>If you have a Facebook <b>page</b> with a URL like this: <code>https://www.facebook.com/your_page_name</code> then the Page ID is just <b>your_page_name</b>.</p>',
    ];

    //=================== Access Token =====================
    $form['feed_settings']['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access token for your page'),
      '#description' => $this->t('Access token needed to deal with the Facebook API'),
      '#default_value' => $this->configuration['access_token'],
      '#maxlength' => 225,
      '#size' => 50,
      '#required' => TRUE,
      '#weight' => '10',
    ];

    $form['feed_settings']['access_token_info'] = [
      '#type' => 'details',
      '#title' => $this->t('What is an access token?'),
      '#weight' => '11',
    ];
    $form['feed_settings']['access_token_info']['summary'] = [
      '#markup' => '<p>A Facebook Access Token is not required to use this module, but we recommend it so that you are not reliant on the token built into the module.</p>'
        . '<p>If you have your own token then you can enter it here.</p>'
        . '<p>To get your own Access Token you can follow these step-by-step instructions.</p>',
    ];
    //======================================================
    $form['feed_settings']['limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Posts limit'),
      '#description' => $this->t('The maximum 30 and min 3 posts.'),
      '#default_value' => $this->configuration['limit'],
      '#min' => 3,
      '#max' => 30,
      '#step' => 1,
      '#size' => 3,
      '#weight' => '10',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['app_id'] = $form_state->getValue('feed_settings')['app_id'];
    $this->configuration['app_secret'] = $form_state->getValue('feed_settings')['app_secret'];
    $this->configuration['page_id'] = $form_state->getValue('feed_settings')['page_id'];
    $this->configuration['access_token'] = $form_state->getValue('feed_settings')['access_token'];
    $this->configuration['limit'] = $form_state->getValue('feed_settings')['limit'];
  }

  /**
   * {@inheritdoc}
   */
  function build() {
    $posts = NULL;
    $res = [];
    \Drupal::service('page_cache_kill_switch')->trigger();

    $fb = new Facebook([
      'app_id' => $this->configuration['app_id'],
      'app_secret' => $this->configuration['app_secret'],
      'default_graph_version' => 'v3.2'
    ]);

    $access_token = $this->configuration['access_token'];
    $page = $this->configuration['page_id'];
    $limit = $this->configuration['limit'];

    try {
      $response = $fb->get("/$page?fields=posts.limit($limit){attachments,created_time}", $access_token);
      $posts = $response->getGraphNode();
    } catch (FacebookResponseException $e) {
      if ($e->getCode() == 190) {
        \Drupal::logger('facebook')->error('Access is denied: check access token on Facebook!');
      } else {
        \Drupal::logger('facebook')->error('Exception occured, code: @code with message: @message', ['@code' => $e->getCode(), '@message' => $e->getMessage()]);
      }
    }

    if ($posts) {
      $data = json_decode($posts);

      foreach ($data->posts as $keys) {
        if ($keys->attachments[0]->type == 'album') {
          $image = $keys->attachments[0]->subattachments[0]->media->image->src;
        } else {
          $image = (isset($keys->attachments[0]->media->image->src) ? $keys->attachments[0]->media->image->src : '');
        }

        if ($keys->attachments[0]->type == 'photo' || $keys->attachments[0]->type == 'album') {
          $title = '';
        } else {
          $title = (isset($keys->attachments[0]->title)) ? $keys->attachments[0]->title : '';
        }

        $img = [
          '#theme' => 'imagecache_external',
          '#uri' => $image,
          '#style_name' => 'facebook_posts',
          '#alt' => 'Druplicon',
        ];

        $description = (isset($keys->attachments[0]->description)) ? $keys->attachments[0]->description : '';
        $link = (isset($keys->attachments[0]->url)) ? $keys->attachments[0]->url : '#';

        $res[] = [
          'title' => $title,
          'description' => $description,
          'link' => $link,
          'image' => $this->renderer->render($img),
          'created_date' => substr($keys->created_time->date, 0, 10),
          'created_time' => substr($keys->created_time->date, 11, 8),
        ];
      }

      $build = [
        '#theme' => 'facebook_post',
        '#posts' => $res,
        '#status' => 1,
      ];

    } else {

      $node_storage = \Drupal::entityTypeManager()->getStorage('node');

      $query = \Drupal::entityQuery('node')
        ->condition('type', 'my_news');
      $nids = $query->execute();

      $nodes = $node_storage->loadMultiple($nids);

      foreach ($nodes as $node) {
        //$image_file_id = $node->field_image->target_id;
        //$image_file = File::load($image_file_id);
        $uri = ImageStyle::load('facebook_posts')->buildUrl($node->field_image->entity->getFileUri());

        $res[] = [
          'title' => $node->title->value,
          'description' => $node->body->value,
          'link' => 'https://www.facebook.com/karabasbrewery',
          'image' => $uri,
          'created_date' => '2019-01-10',
          'created_time' => '12:48:00',
        ];
      }

      $build = [
        '#theme' => 'facebook_post',
        '#posts' => $res,
        '#status' => 0,
      ];
    }

    // Cache for a day.
    /*$build['#cache']['keys'] = [
      'block',
      'facebook_block',
      $this->configuration['id'],
      $this->configuration['access_token'],
    ];
    $build['#cache']['context'][] = 'languages:language_content';
    $build['#cache']['max-age'] = 15 * 60;*/

    return $build;
  }

}
