<?php

namespace Drupal\karabas_instagram\Plugin\Block;

use Drupal\instagram_block\Plugin\Block\InstagramBlockBlock;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a 'InstagramBlock' block.
 *
 * @Block(
 *  id = "karabas_instagram_block",
 *  admin_label = @Translation("Karabas Instagram block"),
 * )
 */
class InstagramBlock extends InstagramBlockBlock {

  // 10180320306.41371ce.9f8cebc1c2cb4ac7923401edcdfd875b

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'access_token' => '',
      'count' => 4,
      'width' => 150,
      'height' => 150,
      'img_resolution' => 'instagram_posts',
      'cache_time_minutes' => 900,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $styles = ImageStyle::loadMultiple();
    $image_styles = array_keys($styles);
    foreach ($image_styles as $key => $value) {
      $image_options[$value] = $value;
    }

    $form['authorise'] = [
      '#markup' => $this->t('Instagram Block requires connecting to a specific Instagram account. You need to be able to log into that account when asked to. The @help page helps with the setup.', ['@help' => Link::fromTextAndUrl($this->t('Authenticate with Instagram'), Url::fromUri('https://www.drupal.org/node/2746185'))->toString()]),
    ];
    $form['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Token'),
      '#description' => $this->t('Your Instagram access token. Eg. 460786509.ab103e5.a54b6834494643588d4217ee986384a8'),
      '#default_value' => $this->configuration['access_token'],
    ];

    $form['count'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of images to display'),
      '#default_value' => $this->configuration['count'],
    ];

    $form['img_resolution'] = [
      '#type' => 'select',
      '#title' => $this->t('Image resolution'),
      '#description' => $this->t('Choose the quality of the images you would like to display.'),
      '#default_value' => $this->configuration['img_resolution'],
      '#options' => $image_options,
    ];

    $form['cache_time_minutes'] = [
      '#type' => 'number',
      '#title' => $this->t('Cache time in minutes'),
      '#description' => $this->t("Default is 1440 - 24 hours. This is important for performance reasons and so the Instagram API limits isn't reached on busy sites."),
      '#default_value' => $this->configuration['cache_time_minutes'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Build a render array to return the Instagram Images.
    $build = [
      'children' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['instagram-wrapper'],
        ],
      ],
    ];

    // If no configuration was saved, don't attempt to build block.
    if (empty($this->configuration['access_token'])) {
      // @TODO Display a message instructing user to configure module.
      return $build;
    }

    // Build url for http request.
    $uri = "https://api.instagram.com/v1/users/self/media/recent/";
    $options = [
      'query' => [
        'access_token' => $this->configuration['access_token'],
        'count' => $this->configuration['count'],
      ],
    ];
    $url = Url::fromUri($uri, $options)->toString();

    // Get the instagram images and decode.
    $result = $this->fetchData($url);
    if (!$result) {
      return $build;
    }

    foreach ($result['data'] as $post) {
      $img_origin = [
        '#theme' => 'imagecache_external',
        '#uri' => $post['images']['low_resolution']['url'],
        '#style_name' => $this->configuration['img_resolution'],
        '#alt' => 'Druplicon',
      ];

      $post['img_origin'] = render($img_origin);

      $build['children'][$post['id']] = [
        '#theme' => 'instagram_block_image',
        '#data' => $post,
        '#href' => $post['link'],
      ];
    }

    // Add css.
    if (!empty($build)) {
      $build['#attached']['library'][] = 'instagram_block/instagram_block';
    }

    // Cache for a day.
    $build['#cache']['keys'] = [
      'block',
      'instagram_block',
      $this->configuration['id'],
      $this->configuration['access_token'],
    ];
    $build['#cache']['context'][] = 'languages:language_content';
    $build['#cache']['max-age'] = $this->configuration['cache_time_minutes'] * 60;

    return $build;
  }

}
