(function ($, Drupal) {
  Drupal.behaviors.basic_cart = {
    attach: function (context, settings) {

      $('table tr').each(function(row){
        $(this).find('.views-field-field-order-status').each(function(){
          var text = $.trim($(this).text());

          switch (text) {
            case 'Оброблено':
              $(this).parent().addClass('processed');
              break;
            case 'В очікуванні':
              $(this).parent().addClass('pending');
              break;
            case 'В обробці':
              $(this).parent().addClass('in-processing');
              break;
            case 'Відмінено':
              $(this).parent().addClass('canceled');
              break;
          }
        });
      });
    }
  };
})(jQuery, Drupal);