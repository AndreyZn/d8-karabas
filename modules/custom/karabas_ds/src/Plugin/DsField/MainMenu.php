<?php

namespace Drupal\karabas_ds\Plugin\DsField;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\karabas_paragraphs\Plugin\paragraphs\Behavior\AnchorParagraphBehavior as Anchor;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * DS field which render Main menu.
 *
 * @DsField(
 *   id = "karabas_main_menu",
 *   title = @Translation("DS: Main menu"),
 *   provider = "karabas_ds",
 *   entity_type = "node",
 *   ui_limit = {"page|default"}
 * )
 */
class MainMenu extends DsFieldBase {

  /**
   * The menu link tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuTree;

  /**
   * Menu id.
   *
   * @var string
   */
  const MENU_NAME = 'main';

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MenuLinkTreeInterface $menu_tree) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->menuTree = $menu_tree;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu.link_tree')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $parameters = $this->menuTree->getCurrentRouteMenuTreeParameters(self::MENU_NAME);

    // Adjust the menu tree parameters based on the block's configuration.
    $level = $config['level'];
    $depth = $config['depth'];
    $parameters->setMinDepth($level);
    // When the depth is configured to zero, there is no depth limit. When depth
    // is non-zero, it indicates the number of levels that must be displayed.
    // Hence this is a relative depth that we must convert to an actual
    // (absolute) depth, that may never exceed the maximum depth.
    if ($depth > 0) {
      $parameters->setMaxDepth(min($level + $depth - 1, $this->menuTree->maxDepth()));
    }

    // For menu blocks with start level greater than 1, only show menu items
    // from the current active trail. Adjust the root according to the current
    // position in the menu in order to determine if we can show the subtree.
    if ($level > 1) {
      if (count($parameters->activeTrail) >= $level) {
        // Active trail array is child-first. Reverse it, and pull the new menu
        // root based on the parent of the configured start level.
        $menu_trail_ids = array_reverse(array_values($parameters->activeTrail));
        $menu_root = $menu_trail_ids[$level - 1];
        $parameters->setRoot($menu_root)->setMinDepth(1);
        if ($depth > 0) {
          $parameters->setMaxDepth(min($level - 1 + $depth - 1, $this->menuTree->maxDepth()));
        }
      }
    }

    $tree = $this->menuTree->load(self::MENU_NAME, $parameters);
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $this->menuTree->transform($tree, $manipulators);
    $build = $this->menuTree->build($tree);

    if (!isset($build['#items'])) {
      $build = [
        '#cache' => [
          'contexts' => ['user.permissions'],
          'tags' => ['config:system.menu.main'],
        ],
        '#sorted' => TRUE,
        '#theme' => 'menu__main',
        '#menu_name' => 'main',
        '#items' => [],
      ];
    }

    $menu_links = $this->getAnchorMenuLinks();
    $menu_links_delivery = $this->getAnchorMenuLinksDelivery();

    $build['#items'] = isset($build['#items']) ? $this->addWeightElement($build['#items']) : [];
    $build['#items'] = array_merge($build['#items'], $menu_links, $menu_links_delivery);

    uasort($build['#items'], [
      '\Drupal\Component\Utility\SortArray',
      'sortByWeightElement'
    ]);

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->entity();

    // Merge menu and node cache tags.
    if (empty($build['#cache']['tags'])) {
      $build['#cache']['tags'] = [];
    }
    $build['#cache']['tags'] = array_merge($build['#cache']['tags'], $node->getCacheTags());

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings) {
    $config = $this->getConfiguration();

    return [
      $this->t('Menu level: @level', ['@level' => $config['level']]),
      $this->t('Menu depth: @depth', ['@depth' => $config['depth']]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'level' => 1,
      'depth' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $form['menu_levels'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Menu levels'),
    ];

    $options = range(0, $this->menuTree->maxDepth());
    unset($options[0]);

    $form['menu_levels']['level'] = [
      '#type' => 'select',
      '#title' => $this->t('Initial visibility level'),
      '#default_value' => $config['level'],
      '#options' => $options,
      '#description' => $this->t('The menu is only visible if the menu item for the current page is at this level or below it. Use level 1 to always display this menu.'),
      '#required' => TRUE,
    ];

    $options[0] = $this->t('Unlimited');

    $form['menu_levels']['depth'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of levels to display'),
      '#default_value' => $config['depth'],
      '#options' => $options,
      '#description' => $this->t('This maximum number includes the initial level.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * Generate anchor links from paragraphs.
   *
   * @return array
   *   Anchor menu links.
   */
  protected function getAnchorMenuLinks() {
    $links = [];
    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->entity();

    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    foreach ($node->field_paragraphs->referencedEntities() as $paragraph) {
      $settings = $paragraph->getBehaviorSetting(Anchor::PLUGIN_ID, Anchor::STATUS_FIELD);

      if ($settings) {
        $title = $paragraph->getBehaviorSetting(Anchor::PLUGIN_ID, Anchor::TITLE_FIELD);
        $weight = $paragraph->getBehaviorSetting(Anchor::PLUGIN_ID, Anchor::WEIGHT_FIELD);

        $links[] = [
          'is_expanded' => FALSE,
          'is_collapsed' => FALSE,
          'in_active_trail' => FALSE,
          'attributes' => new Attribute([
            'class' => [
              'anchor-menu-item',
            ],
          ]),
          'title' => $title,
          'url' => Url::fromUserInput('#' . Anchor::getParagraphId($paragraph)),
          'below' => [],
          'weight' => $weight,
        ];
      }
    }

    return $links;
  }

  protected function getAnchorMenuLinksDelivery() {
    $links = [];

    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->entity();

    $node_id = $node->id();

    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $node = $node_storage->load($node_id);

    foreach ($node->field_add_menu as $item_menu => $value) {
      $links[] = [
        'is_expanded' => FALSE,
        'is_collapsed' => FALSE,
        'in_active_trail' => FALSE,
        'attributes' => new Attribute([
          'class' => [
            'anchor-menu-item',
          ],
        ]),
        'title' => $value->title,
        'url' => $value->uri,
        'below' => [],
        'weight' => $item_menu,
      ];
    }

    return $links;
  }

  /**
   * Add weight element for links.
   *
   * @param array $links
   *   Array of menu links.
   *
   * @return array
   *   Array of menu links with weight element.
   */
  protected function addWeightElement(array $links) {
    foreach ($links as &$link) {
      /** @var \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent $original_link */
      $original_link = $link['original_link'];

      $link['weight'] = $original_link->getWeight();
    }

    return $links;
  }

}
