<?php

namespace Drupal\karabas_ds\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * DS field which render Main menu.
 *
 * @DsField(
 *   id = "karabas_video",
 *   title = @Translation("DS: Video"),
 *   provider = "karabas_ds",
 *   entity_type = "paragraph",
 *   ui_limit = {"slide|*"}
 * )
 */
class Video extends DsFieldBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $youtube_video_id = '';
    $entity = $this->entity();
    $videos = $entity->get('field_video')->getValue();

    if (!empty($videos)) {
      $video_metadata = unserialize($videos[0]['data']);

      if (isset($video_metadata['id'])) {
        $youtube_video_id = $video_metadata['id'];
      }
    }

    return [
      '#type' => 'container',
      [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['youtube-iframe-container'],
          'data-id' => $youtube_video_id,
        ],
        '#markup' => ''
      ],
      [
        '#theme' => 'image',
        '#uri' => 'https://img.youtube.com/vi/' . $youtube_video_id . '/maxresdefault.jpg',
        '#alt' => t('Youtube preview'),
        '#title' => '',
        '#attributes' => [
          'class' => ['youtube-iframe-preview'],
        ],
      ]
    ];
  }

  /**
   * Gets video ID from Youtube url.
   *
   * @param $url
   *   Youtube video url.
   *
   * @return array
   */
  protected function getYoutubeId($url) {
    $regular_expressions = [
      "@(?:(?<protocol>http|https):)?//(?:www\.)?youtube(?<cookie>-nocookie)?\.com/embed/(?<id>[a-z0-9_-]+)@i",
      "@(?:(?<protocol>http|https):)?//(?:www\.)?youtube(?<cookie>-nocookie)?\.com/v/(?<id>[a-z0-9_-]+)@i",
      "@(?:(?<protocol>http|https):)?//(?:www\.)?youtube(?<cookie>-nocookie)?\.com/watch(\?|\?.*\&)v=(?<id>[a-z0-9_-]+)@i",
      "@(?:(?<protocol>http|https):)?//youtu(?<cookie>-nocookie)?\.be/(?<id>[a-z0-9_-]+)@i"
    ];

    foreach ($regular_expressions as $reqular_expr) {
      if (preg_match($reqular_expr, $url, $matches)) {
        return $matches;
      }
    }

    return NULL;
  }

}
