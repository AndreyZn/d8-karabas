<?php

namespace Drupal\karabas_events\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class KarabasRedirectSubscriber
 *
 * Redirect after create node or view node type client_order or addition
 *
 * @package Drupal\karabas_events\EventSubscriber
 */
class KarabasRedirectSubscriber implements EventSubscriberInterface {

  /**
   * Array subscriber events
   *
   * @return array
   */
  public static function getSubscribedEvents() {
    return([
      KernelEvents::REQUEST => [
        ['redirectToListNode'],
      ]
    ]);
  }

  /**
   * Callback function for event
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   */
  public function redirectToListNode(GetResponseEvent $event) {
    $request = $event->getRequest();

    // Проверяем есть ли ключ '_route'
    if (!empty($request->attributes->get('_route'))) {
      // Проверяем является ли ссылка канонической - edit, delete
      if ($request->attributes->get('_route') == 'entity.node.canonical') {
        // Проверяем есть ли ключ 'node'
        if (!empty($request->attributes->get('node'))) {
          $type_node = $request->attributes->get('node')->getType();
          // Проверяем тип контента
          if ($type_node == 'client_order' || $type_node == 'addition') {
            $event->setResponse(new RedirectResponse('/admin/content'));
          }
        }
      }
    }
  }
}