<?php

namespace Drupal\karabas_ajax\Plugin\Block;


use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Term;

/**
 * @Block(
 *   id="product_sidebar_menu",
 *   admin_label=@Translation("Product sidebar ajax menu")
 * )
 */
class BlockSidebarMenu extends BlockBase {

  public function build() {
    $vocabulary_name = 'tovar';
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vocabulary_name);
    $query->sort('weight');

    $tids = $query->execute();
    $terms = Term::loadMultiple($tids);
    //$output = '<ul>';
    $content = [];

    foreach ($terms as $term) {
      $name = $term->getName();
      $id = $term->id();

      $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
        'field_product' => $id,
      ]);

      if (!empty($nodes)) {
        $view_builder = \Drupal::entityTypeManager()->getViewBuilder('taxonomy_term');
        $view = $view_builder->viewField($term->get('field_mobile_image'), [
          'type' => 'string',
          'label' => 'hidden'
        ]);
        $view_render = render($view);

        //$url_object = Url::fromRoute('karabas_ajax.link_tags', ['id' => $id]);
        /*$link = [
          '#type' => 'link',
          '#url' => $url_object,
          '#title' => $this->t($name),
          '#options' => [
            'attributes' => [
              'class' => ['use-ajax']
            ]
          ],
          '#attached' => ['library' => ['core/drupal.dialog.ajax']],
        ];*/

        $content[] = [
          'link' => $id,
          'name' => $name,
          'image' => $view_render,
        ];

        //$output .= '<li>' . $view_render . render($link) . '</li>';
      }
    }

    return [
      '#theme' => 'karabas_ajax_block_sidebar_menu',
      '#content' => $content
    ];

    //$output .= '</ul>';
    //print $output;
  }

}