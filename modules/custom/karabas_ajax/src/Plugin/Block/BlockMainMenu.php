<?php

namespace Drupal\karabas_ajax\Plugin\Block;


use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\taxonomy\Entity\Term;

/**
 * @Block(
 *   id="product_main_menu",
 *   admin_label=@Translation("Product ajax main menu")
 * )
 */
class BlockMainMenu extends BlockBase {

  public function build() {
    $vocabulary_name = 'tovar';
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vocabulary_name);

    $tids = $query->execute();
    $terms = Term::loadMultiple($tids);
    $content = [];

    foreach ($terms as $term) {
      $name = $term->getName();
      $id = $term->id();

      $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
        'field_product' => $id,
      ]);

      if (!empty($nodes)) {
        //====================================

        $display_options = [
          'label'    => 'hidden',
          'type'     => 'responsive_image',
          'settings' => [
            'responsive_image_style' => 'mobile_breakpoint',
          ],
        ];

        $image = $term->get('field_zobrazhennya_tovaru')->view($display_options);
        $image = render($image);

        $icon = $term->get('field_mobile_image')->view([
          'type' => 'string',
          'label' => 'hidden'
        ]);
        $icon = render($icon);

        //====================================

        $content[] = [
          'link' => $id,
          'name' => $name,
          'icon' => $icon,
          'image' => $image,
        ];
      }
    }

    return [
      '#theme' => 'karabas_ajax_block_main_menu',
      '#content' => $content
    ];
  }
}