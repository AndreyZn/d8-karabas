<?php

namespace Drupal\karabas_ajax\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Term;

class ProductController extends ControllerBase {

  public function ajaxViewDodatky($id) {
    $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
      'field_addition' => $id,
    ]);

    $output = '';
    foreach ($nodes as $node) {
      $nid = $node->id();

      $builder = \Drupal::entityTypeManager()->getViewBuilder('node');
      $storage = \Drupal::entityTypeManager()->getStorage('node');
      $node = $storage->load($nid);

      $build = $builder->view($node, 'basic_cart_order');
      $output .= render($build);
      $rebuild_data = "<div class='additionals-wrapper'>" .$output. "</div>";
    }

    $responce = new AjaxResponse();
    $responce->addCommand(new OpenModalDialogCommand($this->t('Additional'), $rebuild_data , ['width' => '400', 'height' => 'auto', 'max-height' => '708px']));

    return $responce;
  }

  /**
   * Display products catalog
   *
   * @param $id
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function ajaxViewProduct($id) {
    $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
      'field_product' => $id,
    ]);

    $output = '';
    foreach ($nodes as $node) {
      $nid = $node->id();

      $builder = \Drupal::entityTypeManager()->getViewBuilder('node');
      $storage = \Drupal::entityTypeManager()->getStorage('node');
      $node = $storage->load($nid);

      $build = $builder->view($node, 'basic_cart_order');
      $output .= render($build);

      if ($node->field_suputni_tovari->target_id) {
        $tid = $node->field_suputni_tovari->target_id;
        $term = Term::load($tid);
        $name_term = $term->name->value;

        $url_object = Url::fromRoute('karabas_ajax.link_dodatky', ['id' => $tid]);
        $link = [
          '#type' => 'link',
          '#url' => $url_object,
          '#title' => $this->t($name_term),
          '#options' => [
            'attributes' => [
              'class' => ['use-ajax']
            ]
          ],
          '#attached' => ['library' => ['core/drupal.dialog.ajax']],
        ];

        $output .= render($link);
      }

    }

    $term = Term::load($id);
    $name = $term->getName();
    $icon = $term->get('field_mobile_image')->view([
      'type' => 'string',
      'label' => 'hidden'
    ]);
    $icon = render($icon);

    $item = [
      'link' => $id,
      'image' => $icon,
      'name' => $name,
    ];

    $build = ['#theme' => 'karabas_ajax_block_sidebar_menu_link', '#item' => $item];
    $rebuild_data3 = \Drupal::service('renderer')->render($build);

    $responce = new AjaxResponse();
    $selector = '.product-block-catalog';
    $selector2 = '.product-link-' . $id;
    $selector3 = '.check-item';
    $method = 'addClass';
    $arguments=['active'];
    $rebuild_data = "<div class='product-block-catalog'>" .$output. "</div>";

    $responce->addCommand(new InvokeCommand($selector2, $method, $arguments));
    $responce->addCommand(new ReplaceCommand($selector3, $rebuild_data3, $settings = NULL));
    $responce->addCommand(new ReplaceCommand($selector, $rebuild_data, $settings = NULL));

    return $responce;
  }

  /*public static function render($theme_hook = 'basic_cart_cart_template', $variable = NULL) {
    $build = ['#theme' => $theme_hook, '#basic_cart' => $variable ? $variable : self::getCartData()];
    return \Drupal::service('renderer')->render($build);
  }*/
}